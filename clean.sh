find . -name ".idea" -type d -exec rm -rf \{\} \;
find . -name "target" -type d -exec rm -rf \{\} \;
find . -name "*.iml" -exec rm \{\} \;
find . -name "*~" -exec rm \{\} \;
