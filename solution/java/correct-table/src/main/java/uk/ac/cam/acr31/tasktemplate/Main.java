package uk.ac.cam.acr31.tasktemplate;


public class Main {

  public static void main(String[] args) {
    Fibonacci f = new Fibonacci();
    for (int i = 0; i < 10; i++) {
      System.out.printf("%01d\t%d%n", i, f.fib(i));
    }
  }
}
