package uk.ac.cam.acr31.tasktemplate;

import java.util.HashMap;
import java.util.Map;

class Fibonacci {

  private final Map<Integer, Integer> cache = new HashMap<>();

  int fib(int i) {
    if (i < 2) {
      return 1;
    }
    if (cache.containsKey(i)) {
      return cache.get(i);
    }
    int result = fib(i - 1) + fib(i - 2);
    cache.put(i, result);
    return result;
  }
}
