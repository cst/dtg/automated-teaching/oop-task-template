package uk.ac.cam.acr31.tasktemplate;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;

public class FibonacciTest {

  @Test
  public void main_doesnt_throw() {
    // ACT
    Main.main(new String[] {});
  }

  @Test
  public void fib_returnsCorrectAnswer() {
    // ARRANGE
    Fibonacci fibonacci = new Fibonacci();

    // ACT
    int result = fibonacci.fib(9);

    // ASSERT
    assertThat(result).isEqualTo(55);
  }
}
