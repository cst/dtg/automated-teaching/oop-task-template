package uk.ac.cam.acr31.tasktemplate;

class Fibonacci {

  int fib(int i) {
    if (i == 0) {
      return 1;
    }
    return fib(i - 1) + fib(i - 2);
  }
}
