Recursive Fibonacci
-------------------

Complete the implementation of `fib(int n)` in the `Fibonacci` class using recursion. 

For the purposes of this exercise you should return 1 for n<2. 