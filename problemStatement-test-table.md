Table Fibonacci
-------------------

The recursive implementation of Fibonacci does lots of extra duplicated computation. This
can be avoided by storing a table to cache previously computed values. This is a classic 
example of making a tradeoff between computation cost and memory usage. 

Add another method to your Fibonacci class with signature `int fibTable(int n)` which uses 
a table to cache previously computed values. Store your table as a field in the class so 
that subsequent calls benefit from the cache too.