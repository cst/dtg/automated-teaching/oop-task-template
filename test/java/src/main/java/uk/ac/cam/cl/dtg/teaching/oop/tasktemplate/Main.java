package uk.ac.cam.cl.dtg.teaching.oop.tasktemplate;

import uk.ac.cam.cl.dtg.teaching.accessor.ConsoleLoggingListener;
import uk.ac.cam.cl.dtg.teaching.accessor.JUnitWrapper;

public class Main {

  public static void main(String[] args) {
    JUnitWrapper.runTestsAndExitVm(new ConsoleLoggingListener(), FibonacciTest.class);
  }
}
