package uk.ac.cam.cl.dtg.teaching.oop.tasktemplate;

import static com.google.common.truth.Truth.assertThat;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import uk.ac.cam.cl.dtg.teaching.accessor.Accessor;
import uk.ac.cam.cl.dtg.teaching.accessor.Purpose;
import uk.ac.cam.cl.dtg.teaching.accessor.TestContext;

@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FibonacciTest {

  interface Fibonacci {
    int fib(int i);
  }

  @Rule public TestContext testContext = new TestContext();

  @Test
  @Purpose("Check that the fib function computes the correct result for a sample value.")
  public void fibonacci_computesCorrectResult() {
    Accessor accessor = testContext.accessor();

    Fibonacci fib =
        accessor.construct("fib", Fibonacci.class, "uk.ac.cam.[^\\.]*.tasktemplate.Fibonacci");

    int result = fib.fib(9);

    assertThat(result).isEqualTo(55);
  }

  @Test
  @Purpose("Check that fib doens't crash with a negative input.")
  public void fibonacci_isRobustToNegativeInput() {
    Accessor accessor = testContext.accessor();

    testContext.testActionListener().mute("Doing a lot of tedious stuff...");
    Fibonacci fib =
        accessor.construct("fib", Fibonacci.class, "uk.ac.cam.[^\\.]*.tasktemplate.Fibonacci");

    int result = fib.fib(-1);
    testContext.testActionListener().unmute();

    assertThat(result).isEqualTo(1);
  }
}
