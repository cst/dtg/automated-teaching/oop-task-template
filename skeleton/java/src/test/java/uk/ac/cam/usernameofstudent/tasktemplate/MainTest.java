package uk.ac.cam.usernameofstudent.tasktemplate;

import org.junit.Test;

public class MainTest {
  @Test
  public void main_doesnt_throw() {
    // ACT
    Main.main(new String[] {});
  }
}
